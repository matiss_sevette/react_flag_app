import React, { useState } from 'react';
import Logo from "../components/Logo";
import Navigation from "../components/Navigation";

let count = 0;

const Counter = () => {
    const [count, setCount] = useState(0);

    return (
      <div>
        <Logo />
        <Navigation />
        <h2>Counter</h2>
        <ul defaultValue={count}>{count}</ul>
        <button onClick={() => setCount(count+1)}>Increment</button>
        <button onClick={() => setCount(count-1)}>Decrement</button>
        <button onClick={() => setCount(0)}>Reset</button>
      </div>
    );
};

export default Counter;