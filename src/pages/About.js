import React from 'react';
import Logo from '../components/Logo';
import Navigation from '../components/Navigation';

const About = () => {
    return (
        <div>
            <Logo />
            <Navigation />
            <h1>A propos</h1>
            <br />
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit autem dolor ratione rem rerum laborum quia aliquid fugiat nemo? Illum voluptatum molestias molestiae, atque veritatis adipisci sit voluptate commodi ullam laudantium asperiores debitis expedita maxime laborum optio eos iste minus reprehenderit. Rerum unde, corporis temporibus adipisci, qui perferendis repudiandae sunt corrupti fugit voluptates quisquam voluptate voluptatum explicabo perspiciatis sit eligendi iure obcaecati beatae facilis illo quod fugiat aliquid. Placeat suscipit eveniet, eos, odit nihil numquam qui repudiandae reiciendis sint distinctio dignissimos repellat praesentium architecto, expedita atque recusandae voluptatum eum quasi id ratione culpa perspiciatis ex dolor! Fugit veritatis blanditiis optio!
            </p>
            <br />
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae distinctio omnis asperiores aspernatur consequuntur animi debitis quis aliquid sunt quam. Qui quas architecto consequuntur facilis ullam, minima ad odio tempore quam repellat veritatis neque dolorum, nam voluptas consequatur magni aliquid, iusto vel assumenda facere commodi debitis nostrum sed illo? Sunt.
            </p>
        </div>
    );
};

export default About;